import { expect } from 'chai';


export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 5000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime);
}

export function checkResponseSchema(response, schema, schemaType: string = 'specified schema') {
    expect(response.body, `Response data should match the ${schemaType} for current request`).to.be.jsonSchema(schema);
}

export function checkFieldNotBeNull(field, fieldName: string) {
    expect(field, `The ${fieldName} field should not be empty`).to.not.be.null;
}

export function checkFieldToBeString(field, fieldName: string) {
    expect(field, `The ${fieldName} field should be a string`).to.be.a('string');
}

export function checkFieldToBeGreaterZero(field, fieldName: string) {
    expect(field.length, `The ${fieldName} field length should be greater than zero`).to.be.greaterThan(0);
}

export function checkFieldToMatch(field, fieldName: string) {
    expect(field, `The ${fieldName} field should match ^[a-zA-z]{2,40}$`).to.match(/^[a-zA-z]{2,40}$/);
}