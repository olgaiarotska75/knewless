import { ApiRequest } from '../request';

export class UserController {
    async getCurrentUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url('user/me')
            .bearerToken(accessToken)
            .send();
        return response;
    }
}