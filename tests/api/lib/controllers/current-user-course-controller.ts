import { ApiRequest } from '../request';

export class CurrentUserCourseController {
    async startCourse(accessToken: string, courseId: string, userId: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('POST')
            .url(`course/continue/start`)
            .bearerToken(accessToken)
            .body({
                courseId: courseId,
                userId: userId
            })
            .send();
        return response;
    }
}