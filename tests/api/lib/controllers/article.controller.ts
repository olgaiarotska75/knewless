import { ApiRequest } from '../request';

export class ArticleController {
    async saveArticle(accessToken: string, article) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('POST')
            .url('article')
            .bearerToken(accessToken)
            .body(article)
            .send();
        return response;
    }
    async getArticles(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`article/author`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async getArticle(accessToken: string, articleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`article/${articleId}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}