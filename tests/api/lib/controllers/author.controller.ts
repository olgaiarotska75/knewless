import { ApiRequest } from '../request';

export class AuthorController {
    async getSettings(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url('author')
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async getPublicAuthor(accessToken: string, authorId: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`author/overview/${authorId}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async setSettings(accessToken: string, settings: object) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('POST')
            .url('author')
            .bearerToken(accessToken)
            .body(settings)
            .send();
        return response;
    }
}