import { ApiRequest } from '../request';

export class AuthController {
    async authenticateUser(emailVal: string, passwordVal: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('POST')
            .url('auth/login')
            .body({
                email: emailVal,
                password: passwordVal,
            })
            .send();
        return response;
    }
}