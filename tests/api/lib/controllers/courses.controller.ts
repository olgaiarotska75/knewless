import { ApiRequest } from '../request';

export class CoursesController {
    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`course/all`)
            .send();
        return response;
    }

    async getPopularCourses() {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`course/popular`)
            .send();
        return response;
    }

    async getAllCourseInfoById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`course/${id}/info`)
            .send();
        return response;
    }

    async getCourseById(accessToken: string, id: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('GET')
            .url(`course/${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async setRating(accessToken: string, courseId: string, rating: number) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method('POST')
            .url(`course/reaction/${courseId}`)
            .bearerToken(accessToken)
            .body({
                rating: rating
            })
            .send();
        return response;
    }

}