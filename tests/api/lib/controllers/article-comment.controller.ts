import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;
//let baseUrl: string = 'http://knewless.tk/api/';

export class ArticleCommentController {
    async saveComment(accessToken: string, comment) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url('article_comment')
            .bearerToken(accessToken)
            .body(comment)
            .send();
        return response;
    }
    async getCommentsByArticle(accessToken: string, articleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`article_comment/of/${articleId}?size=200`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}