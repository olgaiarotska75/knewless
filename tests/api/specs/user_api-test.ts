import { expect } from 'chai';
import { checkStatusCode,checkResponseTime,checkResponseSchema } from '../../helpers/functionsForChecking.helpers';
import { AuthController } from '../lib/controllers/auth.controller';
import { UserController } from '../lib/controllers/user.controller';
import { CoursesController } from '../lib/controllers/courses.controller';
import { CurrentUserCourseController } from '../lib/controllers/current-user-course-controller';
const auth = new AuthController();
const user = new UserController();
const courses = new CoursesController();
const currentUserCourseController = new CurrentUserCourseController();

const schemas = require("./data/schemas_testData.json");
var chai = require('chai');
chai.use(require('chai-json-schema'));

let accessToken: string, userId: string, courseId: string;

describe('User chain', () => {

    it(`Authenticate user`, async () => {
        let response = await auth.authenticateUser(
            global.appConfig.users.Olga.email, 
            global.appConfig.users.Olga.password
            );
        accessToken = response.body.accessToken;

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_auth, 'schemas.schema_auth');
    });

    it(`Get current user info`, async () => {
        let response = await user.getCurrentUser(accessToken);
        userId = response.body.id;
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_userInfo, 'schemas.schema_userInfo');
    });

    it(`Get all courses`, async () => {
        let response = await courses.getAllCourses();
        courseId = response.body[8].id;
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_allCourses, 'schemas.schema_allCourses');
    });

    it(`Get course by ID`, async () => {
        let response = await courses.getCourseById(accessToken, courseId);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_courseById, 'schemas.schema_courseById');
    });

    it(`Start course`, async () => {
        let response = await currentUserCourseController.startCourse(accessToken, courseId, userId);
            
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_courseStart, 'schemas.schema_courseStart');
    });

    it(`Set Course Rating`, async () => {
        let response = await courses.setRating(accessToken, courseId, 4);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        expect(response.body, 'Response data should match the current rating value').to.be.equal(4);
    });






    
 
    
});
