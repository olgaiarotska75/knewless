import { expect } from 'chai';
import { checkStatusCode,checkResponseTime,checkResponseSchema,checkFieldNotBeNull,checkFieldToBeString,checkFieldToBeGreaterZero,checkFieldToMatch } from '../../helpers/functionsForChecking.helpers';
import { AuthController } from '../lib/controllers/auth.controller';
import { UserController } from '../lib/controllers/user.controller';
import { AuthorController } from '../lib/controllers/author.controller';
import { ArticleController } from '../lib/controllers/article.controller';
import { ArticleCommentController } from '../lib/controllers/article-comment.controller';
const auth = new AuthController();
const user = new UserController();
const author = new AuthorController();
const article = new ArticleController();
const articleComment = new ArticleCommentController();

const schemas = require("./data/schemas_testData.json");
var chai = require('chai');
chai.use(require('chai-json-schema'));
chai.use(require('chai-match'));

let accessToken: string, userId: string, articleId: string;

describe('Author chain with hook', () => {

    before(`Authenticate user`, async () => {
        let response = await auth.authenticateUser(
            global.appConfig.users.TSClub.email, 
            global.appConfig.users.TSClub.password
            );
        accessToken = response.body.accessToken;

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_auth, 'schemas.schema_auth');
        checkFieldToBeString(response.body.accessToken, 'accessToken');
        checkFieldToBeString(response.body.refreshToken, 'refreshToken');
        checkFieldToBeGreaterZero(response.body.accessToken, 'accessToken');
        checkFieldToBeGreaterZero(response.body.refreshToken, 'refreshToken');
    });

    beforeEach(function () {
        console.log('before each');
    });

    afterEach(function () {
        console.log('after each');
    });

    after(function () {
        console.log('after');
    });

    it(`Get settings`, async () => {
        let response = await author.getSettings(accessToken);
        userId = response.body.id;

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response,schemas.schema_authorInfo);
        checkFieldNotBeNull(response.body.id, 'id');
        checkFieldNotBeNull(response.body.firstName, 'firstName');
        checkFieldNotBeNull(response.body.lastName, 'lastName');
        checkFieldNotBeNull(response.body.location, 'location');
        checkFieldToBeString(response.body.id, 'id');
        checkFieldToBeString(response.body.firstName, 'firstName');
        checkFieldToBeString(response.body.lastName, 'lastName');
        checkFieldToBeString(response.body.location, 'location');
        checkFieldToBeString(response.body.biography, 'biography');
        checkFieldToBeString(response.body.twitter, 'twitter');
        checkFieldToBeString(response.body.userId, 'userId');
        checkFieldToBeString(response.body.website, 'website');
        checkFieldToMatch(response.body.firstName, 'firstName')
        checkFieldToMatch(response.body.lastName, 'lastName')
        // expect(response.body.lastName, 'The lastName field should not be match ^[a-zA-z]{2,40}$').to.match(/^[a-zA-z]{2,40}$/);
    });

    it(`Set settings`, async () => {
        let response = await author.setSettings(accessToken, {
            "id": "3199ca25-5f32-488e-b00a-6d5e7f56ecae",
            "userId": "14a49655-b833-4bec-ba34-4fa83177f82b",
            "avatar": null,
            "firstName": "TSa",
            "lastName": "Club",
            "job": null,
            "location": "Belize",
            "company": null,
            "website": "",
            "twitter": "https://twitter.com/",
            "biography": ""
        });

        checkStatusCode(response, 200);
        checkResponseTime(response);
        expect(response.body.message, 'Response Message should be success').to.be.equal('Success. Your profile has been updated.');
    });

    it(`Get current user info`, async () => {
        let response = await user.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_userInfo, 'schemas.schema_userInfo');
    });



    it(`Get author overview`, async () => {
        let response = await author.getPublicAuthor(accessToken, userId);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_authorOverview, 'schemas.schema_authorOverview');
    });

    it(`Create article`, async () => {

        let response = await article.saveArticle(accessToken, {
            authorId: userId,
            authorName: 'TS Club',
            name: 'TS Club Test Article',
            text: 'Aloha, this is the best article',
        });

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_article, 'schemas.schema_article');
    });

    it(`Get all articles`, async () => {

        let response = await article.getArticles(accessToken);
        articleId = response.body[0].id;

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_artilcesAuthor, 'schemas.schema_artilcesAuthor');
    });

    it(`Get article by ID`, async () => {

        let response = await article.getArticle(accessToken, articleId);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_articleById, 'schemas.schema_articleById');
    });

    it(`Create Comment`, async () => {

        let response = await articleComment.saveComment(accessToken, {
            "articleId": articleId,
            "text": "New Comment"
        });

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_articleComment, 'schemas.schema_articleComment');
    });
    
    it(`Get Comments by Article ID`, async () => {

        let response = await articleComment.getCommentsByArticle(accessToken, articleId);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_articleComments, 'schemas.schema_articleComments');
    });
    
});
