import { checkStatusCode,checkResponseTime } from '../../helpers/functionsForChecking.helpers';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

describe('Use test data', () => {
    let invalidCredentialsDataSet = [
        { email: 'tsclub.kyiv@gmail.com', password: '      ' },
        { email: 'tsclub.kyiv@gmail.com', password: 'QWER1234 ' },
        { email: 'tsclub.kyiv@gmail.com', password: ' QWER1234' },
        { email: 'tsclub.kyiv@gmail.com', password: 'admin' },
        { email: 'tsclub.kyiv@gmail.com', password: 'tsclub.kyiv@gmail.com' },
        { email: 'tsclub.kyiv@ gmail.com ', password: 'QWER1234' },
        { email: 'tsclub.kyiv@gmail.com  ', password: 'QWER1234' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseTime(response);
        });
    });
});
