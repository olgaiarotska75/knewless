import { checkStatusCode,checkResponseTime,checkResponseSchema } from '../../helpers/functionsForChecking.helpers';
import { AuthController } from '../lib/controllers/auth.controller';
import { CoursesController } from '../lib/controllers/courses.controller';
const auth = new AuthController();
const courses = new CoursesController();

const schemas = require("./data/schemas_testData.json");
var chai = require('chai');
chai.use(require('chai-json-schema'));

let accessToken: string, courseId: string;

xdescribe('Negative Cases chain', () => {

    it(`Authenticate user`, async () => {
        let response = await auth.authenticateUser(
            global.appConfig.users.Olga.email, 
            global.appConfig.users.Olga.password
            );
        accessToken = response.body.accessToken;

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_auth, 'schemas.schema_auth');
    });

    it(`Get all courses`, async () => {
        let response = await courses.getAllCourses();
        courseId = response.body[8].id;

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseSchema(response, schemas.schema_article, 'schemas.schema_article');
    });
});
