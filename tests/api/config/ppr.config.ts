global.appConfig = {
    envName: 'PPR Environment',
    baseUrl: 'https://knewless.tk/api/',
    swaggerUrl: 'https://knewless.tk/api/swagger-ui/index.html',

    users: {
        TSClub: {
            email: 'tsclub.kyiv@gmail.com',
            password: 'QWER1234',
        },
        Olga: {
            email: 'olgaiarotska75@gmail.com',
            password: 'QWE123',
        }
    },
};
